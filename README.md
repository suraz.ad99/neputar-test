# Getting Started with test 

This project was created with PHP Laravel

## Projet Setup
Clone the repository from master branch

RUN
### `Composer Install`
Installs necessary packages using composer

### `cp .env.example .env`
Copies example env file into your env file\
Create a database and add credentials to your database using\
DB_CONNECTION=mysql\
DB_HOST=127.0.0.1\
DB_PORT=3306\
DB_DATABASE=eventapp\
DB_USERNAME=root\
DB_PASSWORD=

### `php artisan key:generate`
generates application key

### `php artisan migrate`
Runs the migration in your local database

### `php artisan db:seed`
Seeds the necessary tables

### `php artisan serve`
Starts the development server in localhost:8000

## Database
Mysql database is inside SQL folder
Import it and add it to your database


## Q. NO 1 Checkout system
Database Schema is inside database/migrations
Class Diagram is inside UML Diagrams Folder
Relationsips are maintained inside Models folder

## Q. NO 2 Reward System
1. Database Schema is inside database/migrations
2. Class Diagram is inside UML Diagrams Folder
3. Relationsips are maintained inside Models folder
4. Php function is inside Services/RewardService.php

## Q. NO 3 Search System
1. Database Schema is inside database/migrations
2. Factory is inside database/factories for each table
3. Search Query is inside app/Http/Controllers/SearchController and is implemented with pagination
4. results can be checked using postman by requesting localhost:8000/api/student/search with headers accept applicatin/json property. 

## `Enjoy the application`
