<?php

namespace App\Services;

use App\Models\Customer;

class RewardService {

    public function creditUserRewared($orderRequest){

        $orderRequest['amount'] = intval($orderRequest['amount']);
        if($orderRequest['status'] == 'complete'){
            if($orderRequest['currency'] != 'USD'){
                $orderRequest['amount'] = $orderRequest['amount'] /  129; // assume 1 USD = 129 NPR
            }

            $customer = Customer::find($orderRequest['customer_id']);
            $customer->reward->create([
                'points' => $orderRequest['amount'],
                'expiry_date' => date('Y-m-d', strtotime('+1 years'))
            ]);
        }
    }
}