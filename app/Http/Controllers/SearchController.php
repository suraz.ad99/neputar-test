<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function search(Request $request){
        $this->validate($request, [
            'keyword' => 'required'
        ]);

        $keyword = $request->keyword;
        $query = "(SELECT student_name, location, 'email', 'mobile_no' FROM students
            WHERE student_name LIKE '%" . $keyword . "%'
            OR location LIKE '%" . $keyword ."%'
            OR email LIKE '%" . $keyword ."%'
            OR mobile_no LIKE '%" . $keyword ."%'
            ORDER BY created_at DESC
            ) 
           UNION
           (SELECT full_name, address, 'email', 'contact' FROM leads
            WHERE full_name LIKE '%" . $keyword . "%'
            OR address LIKE '%" . $keyword ."%'
            OR email LIKE '%" . $keyword ."%'
            OR contact LIKE '%" . $keyword ."%'
            ORDER BY created_at DESC
            )
           UNION
           (SELECT username, city, 'email', 'phone_no' FROM student_users
           WHERE username LIKE '%" . $keyword . "%'
           OR city LIKE '%" . $keyword ."%'
           OR email LIKE '%" . $keyword ."%'
           OR phone_no LIKE '%" . $keyword ."%'
           ORDER BY created_at DESC
           )
          ";

        $results = collect(DB::select($query));

        return $results->paginate(10);

    }
}
